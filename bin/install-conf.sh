#!/bin/sh

bindir=`cd $(dirname $0) && pwd`
distdir=${bindir%/*}

cp $distdir/conf/nginx-ownCloud /etc/logrotate.d/
cp $distdir/conf/nginx-ownCloud.service /usr/lib/systemd/system/

