#!/bin/sh

set -e

bindir=`cd $(dirname $0) && pwd`
distdir=${bindir%/*}
confdir="${distdir}/conf"

domain=$1
fastcgi=$2

ngxroot=$NGXBREW_ROOT

if [ "$ngxroot" == '' ]; then ngxroot=~/.ngxbrew; fi
  
nginx="${ngxroot}/nginxes/ownCloud/sbin/nginx"

# genearte nginx.conf
xslate -s TTerse \
    -D domain=$domain \
    -D fastcgi=$fastcgi \
    -D distdir=$distdir \
    $confdir/nginx.conf.xt > $confdir/nginx.conf

# geneate nginx-ownCloud.service
xslate -s TTerse \
    -D nginx=$nginx \
    -D distdir=$distdir \
    $confdir/nginx-ownCloud.service.xt > $confdir/nginx-ownCloud.service

# generate logrotate.d/nginx-ownCloud
xslate -s TTerse \
    -D distdir=$distdir \
    $confdir/nginx-ownCloud.xt > $confdir/nginx-ownCloud

