#!/bin/sh

set -e

bindir=`cd $(dirname $0) && pwd`;
distdir=${bindir%/*}

cd $distdir;

BASE_URL='http://owncloud.org/releases'
FILENAME='owncloud'
VERSION='4.5.1'
SUFFIX='tar.bz2'

DISTFILE="${BASE_URL}/${FILENAME}-${VERSION}.${SUFFIX}";

curl $DISTFILE | tar -jxvf -

mkdir -p ${distdir}/root
mv owncloud root/ownCloud


