#!/bin/sh

set -e

bindir=`cd $(dirname $0) && pwd`
distdir=${bindir%/*}

ngxbrew install 1.2.4 ownCloud --enable="http_gzip_static,http_ssl" --configure="--pid-path=${distdir}/logs/nginx.pid --user=${USER} --group=users"
