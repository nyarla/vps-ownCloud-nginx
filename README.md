ownCloud with NGINX installer for on ArchLinux VPS
==================================================

1. Requirements
---------------

* ArchLinux under `systemd`
* `ngxbrew` (NGINX installer written by me)
* `perl` and `xslate` command (`cpanm -v Text::Xslate`)
* `git` (checkout this package)
* `server.crt` and `server.key` (your SSL/TLS certificate files)

2. How to Install
-----------------

### 2.1 checkout this package

    $ mkdir -p ~/service && cd ~/service
    $ git clone git://github.com/nyarla/vps-owncloud-with-nginx.git ownCloud
    $ cd ownCloud

### 2.2 setup nginx

    $ bin/install-nginx.sh

### 2.3 setup ownCloud files

    $ bin/install-ownCloud.sh

### 2.4 generate using files

    $ bin/generate.sh {hostname} {php-fpm sock or address}

### 2.5 set certificate files

    $ mkdir -p ssl
    $ cp {hostname}.crt ssl/server.crt
    $ cp {hostname}.key ssl/server.key

### 2.6 install system file

    # bin/install-conf.sh

3. Run ownCloud with NGINX
--------------------------

    # systemctl enable nginx-ownCloud
    # systemctl start nginx-ownCloud

Finished!

4. Author, License and Contact
------------------------------

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net*

This package is under public domain.
